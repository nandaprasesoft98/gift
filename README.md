# Gift

Gift adalah aplikasi untuk melihat gift dan menambahkannya ke wishlist.

Berikut ini merupakan penjelasan dari aplikasi tersebut.

## Design Pattern

Untuk design pattern yang digunakan pada development aplikasi adalah MVVM (Model - View - View Model). Alasan mengapa menggunakan design pattern ini karena hal tersebut telah mendapatkan dukungan langsung dari Google. Dukungan tersebut disematkan dalam suatu Android Architecture Component yang menjadi bagian dari Android Jetpack.

Selain menggunakan MVVM, juga ditambahkan dengan Clean Architecture yang dimana nantinya terdapat beberapa layer yang membagi cakupan. Cakupan layernya adalah sebagai berikut ini:

### Domain

Domain adalah suatu layer dimana terjadi suatu proses orkestrasi bisnis (bisnis proses). Bagian domain ini merupakan bagian yang murni dan tidak terpengaruh oleh dependency apapun. Untuk ketergantungan, layer Presentation dan Data bergantung pada Domain. Akan tetapi Domain tidak boleh bergantung dengan kedua layer tersebut. Hal tersebut bertujuan agar jika salah satu layer diubah, maka layer yang lain tidak terpengaruh (tidak perlu mengubah layer yang lain).

Domain terdiri dari:

- Use case: Merupakan subbagian layer dimana mewakili apa yang akan dilakukan oleh user pada sistem tersebut.

- Entity: Merupakan subbagian layer dimana mewakili objek yang terlibat.

### Data

Data adalah suatu layer dimana perannya adalah menangani bagian data. Layer ini hanya boleh bergantung pada domain.

Data terdiri dari:

- Data source: Merupakan subbagian layer dimana berperan untuk mengambil data.

- Repository: Merupakan subbagian layer dimana berperan untuk menampung dan mengelola data sebelum dilempar ke domain.

### Mediator

Mediator adalah jembatan antara presentation dan domain yang bertugas untuk menjembatani interaksi antara kedua layer tersebut.

### Presentation

Presentation adalah suatu layer dimana perannya adalah menampilkan hasil dari orkestrasi bisnis.

## Library Yang Digunakan

### Library yang digunakan adalah sebagai berikut.

#### Retrofit 2 + Gson

Library Retrofit 2 berfungsi untuk melakukan proses http request. Library Gson (Google Json) berfungsi untuk melakukan parsing terhadap json yang didapatkan dari proses http request.

#### Epoxy + Paging 3

Epoxy adalah library untuk mempermudah membuat dan menampilkan item yang akan tampil di recycler view tanpa perlu mengetikan boilerplate kode. Sedangkan paging 3 adalah library dari Android Jetpack yang mempermudah untuk membuat paging load data. Antara Epoxy dan paging 3 sekarang sudah bisa berkolaborasi.

#### Coroutine Core dan Testing (Library yang berkaitan dengan coroutine beserta component untuk testing)

Library yang berkaitan dengan coroutine beserta component untuk testing. Coroutine merupakan bentuk async dari kotlin dengan format penulisan yang lebih terstruktur

#### Hilt

Dependency Injection library yang merupakan pengembangan dari Dagger2, menyediakan mekanisme yang lebih sederhana untuk digunakan. Dengan library dependency injection ini, maka memungkinkan untuk mengorganisir dependency dengan mudah walaupun dalam aplikasi yang berskala besar. Hilt kini sudah disupport oleh Android Jetpack sehingga penggunaannya kini tidak diragukan lagi.

#### Glide (Image loader)

Glide adalah library untuk meload image baik yang dari local resources maupun dari remote (url). Dengan menggunakan Glide, maka tidak perlu lagi memikirkan low level untuk menghandling async image loading, karena semuanya telah dibackup disitu.

#### Mockito

Merupakan library test mocking yang membantu untuk mock (membuat seolah-olah itu terjadi) objek, sehingga perilaku objek bisa disesuaikan dengan kondisi test tanpa perlu mengubah keoriginalan dari objek tersebut.

## Tech Spec

### API Level

- Min API Level: API 21

Update: Hal tersebut agar otomatis didukung oleh multidex yang berarti tidak perlu menambahkan library tambahan multidex serta memodifikasi class application itu sendiri. Untuk api dibawah 21 harus menambahkan hal tersebut tadi. Juga selain itu pada Retrofit tidak perlu ditambahkan tls compatibility untuk support dibawah 21.

- Target API Level API 30