package com.nandaprasetio.gift

import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.domain.entity.gift.GiftAttribute
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResultMetadata
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.*

object MockHelper {
    fun mockGiftPagingResult(): PagingResult<Gift> {
        val pagingResultMetadata = PagingResultMetadata(
            totalItems = 20,
            currentPage = 1,
            itemPerPage = 10,
            totalPages = 2
        )
        val dataList: List<Gift> = mutableListOf<Gift>().also { mutableList ->
            (1..pagingResultMetadata.itemPerPage).forEach {
                val giftAttribute = GiftAttribute(
                    id = it,
                    name = "Gift $it",
                    info = "Info $it",
                    description = "Description $it",
                    points = 100,
                    slug = "gifts-$it",
                    stock = 10,
                    images = listOf("https://rgbtest.s3.ap-southeast-1.amazonaws.com/images/gift/full/xiaomi-mi-a2-lite.jpg"),
                    isNew = 0,
                    rating = 5.0f,
                    numOfReviews = 120,
                    isWishlist = 0
                )
                mutableList.add(Gift(it, "gift", giftAttribute))
            }
        }
        return PagingResult(pagingResultMetadata, dataList)
    }

    fun mockGiftDetailModelValueList(): List<BaseModelValue> {
        val pagingResultList: MutableList<BaseModelValue> = mutableListOf()
        val gift = mockGift()
        val giftAttribute: GiftAttribute = gift.giftAttribute
        giftAttribute.also {
            pagingResultList.also { pagingResultList ->
                listOf<BaseModelValue>(
                    ImageWithNewBadgeItemModelValue("image-with-new-badge", it.images[0], 1),
                    SeparatorItemModelValue("separator-1"),
                    TitleAndDescriptionItemModelValue("title-and-description", it.name, null),
                    SeparatorItemModelValue("separator-1"),
                    GiftDetailMainCarouselItemModelValue("gift-detail-main-carousel", it.rating, it.numOfReviews, it.points, it.isWishlist, gift),
                    SeparatorItemModelValue("separator-2"),
                    SubTitleItemModelValue("sub-title-info", "Info"),
                    ExpandedDescriptionItemModelValue("expanded-description-info", it.info, 100),
                    SeparatorItemModelValue("separator-3"),
                    SubTitleItemModelValue("sub-title-description", "Description"),
                    ExpandedDescriptionItemModelValue("expanded-description-description", it.description, 100)
                ).also { list ->
                    pagingResultList.addAll(list)
                }
            }
        }
        return pagingResultList
    }

    fun mockGift(): Gift {
        val giftAttribute = GiftAttribute(
            id = 1,
            name = "Gift 1",
            info = "Info 1",
            description = "Description 1",
            points = 100,
            slug = "gifts-1",
            stock = 10,
            images = listOf("https://rgbtest.s3.ap-southeast-1.amazonaws.com/images/gift/full/xiaomi-mi-a2-lite.jpg"),
            isNew = 0,
            rating = 5.0f,
            numOfReviews = 120,
            isWishlist = 0
        )
        return Gift(giftAttribute.id, "gift", giftAttribute)
    }
}