package com.nandaprasetio.gift.repository

import com.nandaprasetio.gift.CoroutineTestRule
import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.MockHelper
import com.nandaprasetio.gift.data.datasource.giftdatasource.GiftDataSource
import com.nandaprasetio.gift.data.repository.giftrepository.DefaultGiftRepository
import com.nandaprasetio.gift.data.repository.giftrepository.GiftRepository
import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class GiftRepositoryUnitTest {
    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    private lateinit var giftDataSource: GiftDataSource
    private lateinit var giftRepository: GiftRepository

    @Before
    fun init() {
        giftDataSource = mock()
        giftRepository = DefaultGiftRepository(coroutineTestRule.testCoroutineDispatcher, giftDataSource)
    }

    @Test
    fun getGift_isSuccess() {
        runBlocking {
            val pagingResult = MockHelper.mockGiftPagingResult()
            whenever(giftDataSource.getGift(1, 10)).thenReturn(pagingResult)
            pagingResult.data.forEach {
                whenever(giftDataSource.getGiftWishlist(it.id)).thenThrow(NullPointerException())
            }
            val giftLoadDataResult = giftRepository.getGift(1, 10)
            Assert.assertEquals(
                giftLoadDataResult as LoadDataResult.Success,
                LoadDataResult.Success(pagingResult)
            )
        }
    }

    @Test
    fun getGift_isError() {
        runBlocking {
            val throwable: Throwable = IllegalStateException()
            whenever(giftDataSource.getGift(1, 10)).thenThrow(throwable)
            val giftLoadDataResult = giftRepository.getGift(1, 10)
            Assert.assertEquals(
                giftLoadDataResult as LoadDataResult.Error,
                LoadDataResult.Error<PagingResult<Gift>>(throwable)
            )
        }
    }

    @Test
    fun getGiftDetail_isSuccess() {
        runBlocking {
            val gift = MockHelper.mockGift()
            whenever(giftDataSource.getGiftDetail(1)).thenReturn(gift)
            whenever(giftDataSource.getGiftWishlist(1)).thenThrow(NullPointerException())
            val giftLoadDataResult = giftRepository.getGiftDetail(1)
            Assert.assertEquals(giftLoadDataResult, LoadDataResult.Success(gift))
        }
    }

    @Test
    fun getGiftDetail_isError() {
        runBlocking {
            val throwable: Throwable = IllegalStateException()
            whenever(giftDataSource.getGiftDetail(1)).thenThrow(throwable)
            val giftLoadDataResult = giftRepository.getGiftDetail(1)
            Assert.assertEquals(
                giftLoadDataResult as LoadDataResult.Error,
                LoadDataResult.Error<PagingResult<Gift>>(throwable)
            )
        }
    }

    @Test
    fun addGiftToWishlist_isSuccess() {
        runBlocking {
            whenever(giftDataSource.insertGiftWishlist(1)).thenReturn(1)
            val giftLoadDataResult = giftRepository.addGiftToWishlist(1)
            Assert.assertEquals(giftLoadDataResult, LoadDataResult.Success(1L))
        }
    }

    @Test
    fun addGiftToWishlist_isError() {
        runBlocking {
            val throwable: Throwable = IllegalStateException()
            whenever(giftDataSource.insertGiftWishlist(1)).thenThrow(throwable)
            val giftLoadDataResult = giftRepository.addGiftToWishlist(1)
            Assert.assertEquals(
                giftLoadDataResult as LoadDataResult.Error,
                LoadDataResult.Error<PagingResult<Gift>>(throwable)
            )
        }
    }
}