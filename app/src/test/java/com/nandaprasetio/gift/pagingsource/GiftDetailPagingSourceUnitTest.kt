package com.nandaprasetio.gift.pagingsource

import androidx.paging.PagingSource
import com.nandaprasetio.gift.CoroutineTestRule
import com.nandaprasetio.gift.MockHelper
import com.nandaprasetio.gift.MockHelper.mockGiftDetailModelValueList
import com.nandaprasetio.gift.data.database.GiftWishlist
import com.nandaprasetio.gift.data.datasource.giftdatasource.GiftDataSource
import com.nandaprasetio.gift.data.pagingsource.GiftDetailPagingSource
import com.nandaprasetio.gift.data.repository.giftrepository.DefaultGiftRepository
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class GiftDetailPagingSourceUnitTest {
    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    private lateinit var giftDataSource: GiftDataSource
    private lateinit var giftDetailPagingSource: GiftDetailPagingSource

    @Before
    fun init() {
        giftDataSource = mock()
        giftDetailPagingSource = GiftDetailPagingSource(
            DefaultGiftRepository(coroutineTestRule.testCoroutineDispatcher, giftDataSource), 1
        )
    }

    @Test
    fun getGiftDetailPagingResult_appendLoad_isSuccess() {
        runBlocking {
            val gift = MockHelper.mockGift()
            whenever(giftDataSource.getGiftDetail(1)).thenReturn(gift)
            whenever(giftDataSource.getGiftWishlist(1)).thenThrow(NullPointerException())
            val pagingSourceLoadResult = giftDetailPagingSource.load(
                PagingSource.LoadParams.Append(1, 10, false)
            )
            Assert.assertEquals(
                pagingSourceLoadResult,
                PagingSource.LoadResult.Page(
                    data = mockGiftDetailModelValueList(),
                    prevKey = null,
                    nextKey = null
                )
            )
        }
    }

    @Test
    fun getGiftDetailPagingResult_appendLoad_isError() {
        runBlocking {
            val throwable = IllegalStateException()
            whenever(giftDataSource.getGiftDetail(1)).thenThrow(throwable)
            whenever(giftDataSource.getGiftWishlist(1)).thenThrow(NullPointerException())
            val pagingSourceLoadResult = giftDetailPagingSource.load(
                PagingSource.LoadParams.Append(1, 10, false)
            )
            Assert.assertEquals(
                pagingSourceLoadResult,
                PagingSource.LoadResult.Error<Int, BaseModelValue>(throwable)
            )
        }
    }

    @Test
    fun getGiftDetailPagingResult_refreshLoad_isSuccess() {
        runBlocking {
            val pagingResult = MockHelper.mockGift()
            whenever(giftDataSource.getGiftDetail(1)).thenReturn(pagingResult)
            whenever(giftDataSource.getGiftWishlist(1)).thenThrow(NullPointerException())
            val pagingSourceLoadResult = giftDetailPagingSource.load(
                PagingSource.LoadParams.Refresh(1, 10, false)
            )
            Assert.assertEquals(
                pagingSourceLoadResult,
                PagingSource.LoadResult.Page(
                    data = mockGiftDetailModelValueList(),
                    prevKey = null,
                    nextKey = null
                )
            )
        }
    }

    @Test
    fun getGiftDetailPagingResult_refreshLoad_isError() {
        runBlocking {
            val throwable = IllegalStateException()
            whenever(giftDataSource.getGiftDetail(1)).thenThrow(throwable)
            val pagingSourceLoadResult = giftDetailPagingSource.load(
                PagingSource.LoadParams.Refresh(1, 10, false)
            )
            Assert.assertEquals(
                pagingSourceLoadResult,
                PagingSource.LoadResult.Error<Int, BaseModelValue>(throwable)
            )
        }
    }
}