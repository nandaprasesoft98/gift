package com.nandaprasetio.gift.pagingsource

import androidx.paging.PagingSource
import com.nandaprasetio.gift.CoroutineTestRule
import com.nandaprasetio.gift.MockHelper
import com.nandaprasetio.gift.data.datasource.giftdatasource.GiftDataSource
import com.nandaprasetio.gift.data.pagingsource.GiftPagingSource
import com.nandaprasetio.gift.data.repository.giftrepository.DefaultGiftRepository
import com.nandaprasetio.gift.ext.map
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.GiftItemModelValue
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class GiftPagingSourceUnitTest {
    @get:Rule
    val coroutineTestRule = CoroutineTestRule()

    private lateinit var giftDataSource: GiftDataSource
    private lateinit var giftPagingSource: GiftPagingSource

    @Before
    fun init() {
        giftDataSource = mock()
        giftPagingSource = GiftPagingSource(
            DefaultGiftRepository(coroutineTestRule.testCoroutineDispatcher, giftDataSource)
        )
    }

    @Test
    fun getGiftPagingResult_appendLoad_isSuccess() {
        runBlocking {
            val pagingResult = MockHelper.mockGiftPagingResult()
            whenever(giftDataSource.getGift(1, 10)).thenReturn(pagingResult)
            pagingResult.data.forEach {
                whenever(giftDataSource.getGiftWishlist(it.id)).thenThrow(NullPointerException())
            }
            val pagingSourceLoadResult = giftPagingSource.load(
                PagingSource.LoadParams.Append(1, 10, false)
            )
            Assert.assertEquals(
                pagingSourceLoadResult,
                PagingSource.LoadResult.Page<Int, BaseModelValue>(
                    data = pagingResult.map { GiftItemModelValue("gift-${it.id}", it) }.data,
                    prevKey = null,
                    nextKey = 2
                )
            )
        }
    }

    @Test
    fun getGiftPagingResult_appendLoad_isError() {
        runBlocking {
            val throwable = IllegalStateException()
            whenever(giftDataSource.getGift(1, 10)).thenThrow(throwable)
            val pagingSourceLoadResult = giftPagingSource.load(
                PagingSource.LoadParams.Append(1, 10, false)
            )
            Assert.assertEquals(
                pagingSourceLoadResult,
                PagingSource.LoadResult.Error<Int, BaseModelValue>(throwable)
            )
        }
    }

    @Test
    fun getGiftPagingResult_refreshLoad_isSuccess() {
        runBlocking {
            val pagingResult = MockHelper.mockGiftPagingResult()
            whenever(giftDataSource.getGift(1, 10)).thenReturn(pagingResult)
            pagingResult.data.forEach {
                whenever(giftDataSource.getGiftWishlist(it.id)).thenThrow(NullPointerException())
            }
            val pagingSourceLoadResult = giftPagingSource.load(
                PagingSource.LoadParams.Refresh(1, 10, false)
            )
            Assert.assertEquals(
                pagingSourceLoadResult,
                PagingSource.LoadResult.Page<Int, BaseModelValue>(
                    data = pagingResult.map { GiftItemModelValue("gift-${it.id}", it) }.data,
                    prevKey = null,
                    nextKey = 2
                )
            )
        }
    }

    @Test
    fun getGiftPagingResult_refreshLoad_isError() {
        runBlocking {
            val throwable = IllegalStateException()
            whenever(giftDataSource.getGift(1, 10)).thenThrow(throwable)
            val pagingSourceLoadResult = giftPagingSource.load(
                PagingSource.LoadParams.Refresh(1, 10, false)
            )
            Assert.assertEquals(
                pagingSourceLoadResult,
                PagingSource.LoadResult.Error<Int, BaseModelValue>(throwable)
            )
        }
    }
}