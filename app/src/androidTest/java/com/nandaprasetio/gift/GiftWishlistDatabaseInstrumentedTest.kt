package com.nandaprasetio.gift

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import com.nandaprasetio.gift.data.database.GiftWishlist
import com.nandaprasetio.gift.data.database.GiftWishlistDatabase
import com.nandaprasetio.gift.data.database.dao.GiftWishlistDao
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@MediumTest
class GiftWishlistDatabaseInstrumentedTest {
    private lateinit var giftWishlistDao: GiftWishlistDao
    private lateinit var giftWishlistDatabase: GiftWishlistDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        giftWishlistDatabase = Room.inMemoryDatabaseBuilder(
            context, GiftWishlistDatabase::class.java
        ).build()
        giftWishlistDao = giftWishlistDatabase.giftWishlistDao()
    }

    @Test
    fun insertAndGetGiftWishlist() {
        val giftWishlist = GiftWishlist(id = 1, giftId = 1)
        giftWishlistDao.insertGiftWishlist(giftWishlist)
        Assert.assertEquals(giftWishlistDao.getGiftWishlist(giftWishlist.giftId), giftWishlist)
    }

    @Test
    fun insertAndDeleteGiftWishlist() {
        val giftWishlist = GiftWishlist(id = 1, giftId = 1)
        giftWishlistDao.insertGiftWishlist(giftWishlist)
        giftWishlistDao.deleteGiftWishlist(giftWishlist.giftId)
        Assert.assertNotEquals(giftWishlistDao.getGiftWishlist(giftWishlist.giftId), giftWishlist)
    }

    @After
    fun closeDb() {
        giftWishlistDatabase.close()
    }
}