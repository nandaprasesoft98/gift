package com.nandaprasetio.gift.data.repository.giftrepository

import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.data.database.GiftWishlist
import com.nandaprasetio.gift.data.datasource.giftdatasource.GiftDataSource
import com.nandaprasetio.gift.di.module.IODispatcher
import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult
import com.nandaprasetio.gift.ext.getHttpRequestResult
import com.nandaprasetio.gift.ext.map
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject

class DefaultGiftRepository @Inject constructor(
    @IODispatcher private val coroutineDispatcher: CoroutineDispatcher,
    private val giftDataSource: GiftDataSource
): GiftRepository {
    override suspend fun getGift(pageNumber: Int, pageSize: Int): LoadDataResult<PagingResult<Gift>> {
        return getHttpRequestResult(coroutineDispatcher) {
            giftDataSource.getGift(pageNumber, pageSize).let {
                it.map { gift -> getGiftWishlistFromGift(gift) }
            }
        }
    }

    override suspend fun getGiftDetail(id: Int): LoadDataResult<Gift> {
        return getHttpRequestResult(coroutineDispatcher) {
            getGiftWishlistFromGift(giftDataSource.getGiftDetail(id))
        }
    }

    override suspend fun addGiftToWishlist(id: Int): LoadDataResult<Long> {
        return getHttpRequestResult(coroutineDispatcher) { giftDataSource.insertGiftWishlist(id) }
    }

    override suspend fun getGiftWishlist(id: Int): LoadDataResult<GiftWishlist> {
        return getHttpRequestResult(coroutineDispatcher) { giftDataSource.getGiftWishlist(id) }
    }

    override suspend fun deleteGiftFromWishlist(id: Int): LoadDataResult<Int> {
        return getHttpRequestResult(coroutineDispatcher) { giftDataSource.deleteGiftWishlist(id) }
    }

    private suspend fun getGiftWishlistFromGift(gift: Gift): Gift {
        return try {
            giftDataSource.getGiftWishlist(gift.id)
            Gift(gift.id, gift.type, gift.giftAttribute.copy(isWishlist = 1))
        } catch (e: NullPointerException) {
            gift
        } catch (e: Throwable) {
            throw e
        }
    }
}