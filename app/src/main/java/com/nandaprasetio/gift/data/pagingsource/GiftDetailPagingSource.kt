package com.nandaprasetio.gift.data.pagingsource

import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.data.repository.giftrepository.GiftRepository
import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResultMetadata
import com.nandaprasetio.gift.ext.map
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.*

class GiftDetailPagingSource(
    private val giftRepository: GiftRepository,
    private val id: Int
): BaseIntKeyedPagingSource<BaseModelValue>() {
    override suspend fun onLoadData(page: Int, pageSize: Int): LoadDataResult<PagingResult<BaseModelValue>> {
        return giftRepository.getGiftDetail(id).let {
            when (it) {
                is LoadDataResult.Success -> it.map { gift ->
                    val pagingResultList: MutableList<BaseModelValue> = mutableListOf()
                    addModelValueToPagingResult(pagingResultList, gift)
                    val pagingResultMetadata: PagingResultMetadata = PagingResultMetadata(
                        totalItems = pagingResultList.size,
                        currentPage = 1,
                        itemPerPage = -1,
                        totalPages = 1
                    )
                    PagingResult(pagingResultMetadata, pagingResultList)
                }
                is LoadDataResult.Error -> it.map()
            }
        }
    }

    private fun addModelValueToPagingResult(
        pagingResultList: MutableList<BaseModelValue>,
        gift: Gift
    ) {
        gift.giftAttribute.also {
            pagingResultList.also { pagingResultList ->
                listOf<BaseModelValue>(
                    ImageWithNewBadgeItemModelValue("image-with-new-badge", it.images[0], 1),
                    SeparatorItemModelValue("separator-1"),
                    TitleAndDescriptionItemModelValue("title-and-description", it.name, null),
                    SeparatorItemModelValue("separator-1"),
                    GiftDetailMainCarouselItemModelValue("gift-detail-main-carousel", it.rating, it.numOfReviews, it.points, it.isWishlist, gift),
                    SeparatorItemModelValue("separator-2"),
                    SubTitleItemModelValue("sub-title-info", "Info"),
                    ExpandedDescriptionItemModelValue("expanded-description-info", it.info, 100),
                    SeparatorItemModelValue("separator-3"),
                    SubTitleItemModelValue("sub-title-description", "Description"),
                    ExpandedDescriptionItemModelValue("expanded-description-description", it.description, 100),
                ).also { list ->
                    pagingResultList.addAll(list)
                }
            }
        }
    }
}