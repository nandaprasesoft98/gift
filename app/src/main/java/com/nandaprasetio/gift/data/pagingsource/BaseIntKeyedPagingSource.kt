package com.nandaprasetio.gift.data.pagingsource

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResultMetadata

abstract class BaseIntKeyedPagingSource<Value: Any>: PagingSource<Int, Value>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Value> {
        val page: Int = params.key ?: 1
        return when (val pagingResultLoadDataResult: LoadDataResult<PagingResult<Value>> = onLoadData(page, params.loadSize)) {
            is LoadDataResult.Success -> {
                val output = pagingResultLoadDataResult.output
                val pagingResultMetadata: PagingResultMetadata = output.pagingResultMetadata
                val nextPage: Int? = (pagingResultMetadata.currentPage + 1).let {
                    if (it > pagingResultMetadata.totalPages) { null } else { it }
                }
                LoadResult.Page(output.data, null, nextPage)
            }
            is LoadDataResult.Error -> {
                LoadResult.Error(pagingResultLoadDataResult.e)
            }
        }
    }

    override fun getRefreshKey(state: PagingState<Int, Value>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    protected abstract suspend fun onLoadData(page: Int, pageSize: Int): LoadDataResult<PagingResult<Value>>
}