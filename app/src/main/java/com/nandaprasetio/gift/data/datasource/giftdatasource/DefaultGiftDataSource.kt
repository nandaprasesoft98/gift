package com.nandaprasetio.gift.data.datasource.giftdatasource

import com.nandaprasetio.gift.data.database.GiftWishlist
import com.nandaprasetio.gift.data.database.dao.GiftWishlistDao
import com.nandaprasetio.gift.data.service.GiftService
import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult
import javax.inject.Inject

class DefaultGiftDataSource @Inject constructor(
    private val giftWishlistDao: GiftWishlistDao,
    private val giftService: GiftService
): GiftDataSource {
    override suspend fun getGift(pageNumber: Int, pageSize: Int): PagingResult<Gift> {
        return giftService.getGift(pageNumber, pageSize)
    }

    override suspend fun getGiftDetail(id: Int): Gift {
        return giftService.getGiftDetail(id)?.gift ?: throw NullPointerException()
    }

    override suspend fun insertGiftWishlist(id: Int): Long {
        return giftWishlistDao.insertGiftWishlist(GiftWishlist(giftId = id))
    }

    override suspend fun getGiftWishlist(id: Int): GiftWishlist {
        return giftWishlistDao.getGiftWishlist(id) ?: throw NullPointerException()
    }

    override suspend fun deleteGiftWishlist(id: Int): Int {
        return giftWishlistDao.deleteGiftWishlist(id)
    }
}