package com.nandaprasetio.gift.data.repository.giftrepository

import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.data.database.GiftWishlist
import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult

interface GiftRepository {
    suspend fun getGift(pageNumber: Int, pageSize: Int): LoadDataResult<PagingResult<Gift>>
    suspend fun getGiftDetail(id: Int): LoadDataResult<Gift>
    suspend fun addGiftToWishlist(id: Int): LoadDataResult<Long>
    suspend fun getGiftWishlist(id: Int): LoadDataResult<GiftWishlist>
    suspend fun deleteGiftFromWishlist(id: Int): LoadDataResult<Int>
}