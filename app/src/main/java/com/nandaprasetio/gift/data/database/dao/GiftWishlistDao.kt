package com.nandaprasetio.gift.data.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.nandaprasetio.gift.data.database.DatabaseContract
import com.nandaprasetio.gift.data.database.GiftWishlist

@Dao
interface GiftWishlistDao {
    @Query("SELECT * FROM ${DatabaseContract.TABLE_NAME_WISHLIST} WHERE ${DatabaseContract.COLUMN_GIFT_ID} = :giftId")
    fun getGiftWishlist(giftId: Int): GiftWishlist?

    @Insert
    fun insertGiftWishlist(giftWishlist: GiftWishlist): Long

    @Query("DELETE FROM ${DatabaseContract.TABLE_NAME_WISHLIST} WHERE ${DatabaseContract.COLUMN_GIFT_ID} = :giftId")
    fun deleteGiftWishlist(giftId: Int): Int
}