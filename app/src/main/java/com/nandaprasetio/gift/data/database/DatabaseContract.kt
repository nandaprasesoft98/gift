package com.nandaprasetio.gift.data.database

object DatabaseContract {
    const val TABLE_NAME_WISHLIST = "wishlist"
    const val COLUMN_GIFT_ID = "gift_id"
    const val DATABASE_NAME = "GiftWishlistDatabase"
}