package com.nandaprasetio.gift.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.nandaprasetio.gift.data.database.dao.GiftWishlistDao

@Database(entities = [GiftWishlist::class], version = 1)
abstract class GiftWishlistDatabase: RoomDatabase() {
    abstract fun giftWishlistDao(): GiftWishlistDao
}