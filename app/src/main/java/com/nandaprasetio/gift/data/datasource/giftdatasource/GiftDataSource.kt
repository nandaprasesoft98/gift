package com.nandaprasetio.gift.data.datasource.giftdatasource

import com.nandaprasetio.gift.data.database.GiftWishlist
import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.domain.entity.gift.GiftDetail
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult

interface GiftDataSource {
    suspend fun getGift(pageNumber: Int, pageSize: Int): PagingResult<Gift>
    suspend fun getGiftDetail(id: Int): Gift
    suspend fun insertGiftWishlist(id: Int): Long
    suspend fun getGiftWishlist(id: Int): GiftWishlist
    suspend fun deleteGiftWishlist(id: Int): Int
}