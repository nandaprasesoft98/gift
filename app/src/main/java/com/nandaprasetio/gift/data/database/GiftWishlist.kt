package com.nandaprasetio.gift.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = DatabaseContract.TABLE_NAME_WISHLIST)
data class GiftWishlist(
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    @ColumnInfo(name = DatabaseContract.COLUMN_GIFT_ID)
    val giftId: Int
)