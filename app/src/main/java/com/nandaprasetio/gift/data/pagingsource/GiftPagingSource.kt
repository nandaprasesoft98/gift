package com.nandaprasetio.gift.data.pagingsource

import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.data.repository.giftrepository.GiftRepository
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult
import com.nandaprasetio.gift.ext.map
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.GiftItemModelValue

class GiftPagingSource(
    private val giftRepository: GiftRepository
): BaseIntKeyedPagingSource<BaseModelValue>() {
    override suspend fun onLoadData(page: Int, pageSize: Int): LoadDataResult<PagingResult<BaseModelValue>> {
        return giftRepository.getGift(page, pageSize).let {
            when (it) {
                is LoadDataResult.Success -> it.map { pagingResult -> pagingResult.map {
                    gift -> GiftItemModelValue("gift-${gift.id}", gift)
                }}
                is LoadDataResult.Error -> it.map()
            }
        }
    }
}