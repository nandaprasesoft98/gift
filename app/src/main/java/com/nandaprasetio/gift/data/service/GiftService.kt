package com.nandaprasetio.gift.data.service

import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.domain.entity.gift.GiftDetail
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

interface GiftService {
    @GET("gifts")
    suspend fun getGift(@Query("page[number]") pageNumber: Int, @Query("page[size]") pageSize: Int): PagingResult<Gift>

    @GET("gifts/{id}")
    suspend fun getGiftDetail(@Path("id") id: Int): GiftDetail?

    @POST("gifts/{id}/wishlist")
    suspend fun addGiftToWishlist(@Path("id") id: Int)
}