package com.nandaprasetio.gift.ext

import com.nandaprasetio.gift.LoadDataResult

inline fun<T, R> LoadDataResult.Success<T>.map(transform: (T) -> R): LoadDataResult<R> {
    return LoadDataResult.Success(transform(this.output))
}

fun<T, R> LoadDataResult.Error<T>.map(): LoadDataResult<R> {
    return LoadDataResult.Error(this.e)
}