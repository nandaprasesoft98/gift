package com.nandaprasetio.gift.ext

import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult

inline fun<T, R> PagingResult<T>.map(transform: (T) -> R): PagingResult<R> {
    return PagingResult(this.pagingResultMetadata, this.data.map(transform))
}