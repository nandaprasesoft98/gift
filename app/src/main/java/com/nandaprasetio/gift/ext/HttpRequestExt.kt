package com.nandaprasetio.gift.ext

import com.nandaprasetio.gift.LoadDataResult
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

suspend fun<T> getHttpRequestResult(
    coroutineDispatcher: CoroutineDispatcher,
    onGetHttpRequestResult: suspend () -> T
): LoadDataResult<T> {
    return try {
        LoadDataResult.Success(withContext(coroutineDispatcher) { onGetHttpRequestResult() })
    } catch (e: Throwable) {
        LoadDataResult.Error(e)
    }
}