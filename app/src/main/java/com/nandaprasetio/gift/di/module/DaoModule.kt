package com.nandaprasetio.gift.di.module

import com.nandaprasetio.gift.data.database.GiftWishlistDatabase
import com.nandaprasetio.gift.data.database.dao.GiftWishlistDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DaoModule {
    @Provides
    @Singleton
    fun provideGiftWishlistDao(giftWishlistDatabase: GiftWishlistDatabase): GiftWishlistDao {
        return giftWishlistDatabase.giftWishlistDao()
    }
}