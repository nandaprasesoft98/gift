package com.nandaprasetio.gift.di.module

import android.content.Context
import androidx.room.Room
import com.nandaprasetio.gift.data.database.DatabaseContract
import com.nandaprasetio.gift.data.database.GiftWishlistDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Provides
    @Singleton
    fun provideGiftWishlistDatabase(@ApplicationContext applicationContext: Context): GiftWishlistDatabase {
        return Room.databaseBuilder(
            applicationContext,
            GiftWishlistDatabase::class.java,
            DatabaseContract.DATABASE_NAME
        ).build()
    }
}