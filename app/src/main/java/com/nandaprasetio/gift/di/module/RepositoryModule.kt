package com.nandaprasetio.gift.di.module

import com.nandaprasetio.gift.data.repository.giftrepository.DefaultGiftRepository
import com.nandaprasetio.gift.data.repository.giftrepository.GiftRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {
    @Binds
    @Singleton
    abstract fun bindDefaultGiftRepository(defaultGiftRepository: DefaultGiftRepository): GiftRepository
}