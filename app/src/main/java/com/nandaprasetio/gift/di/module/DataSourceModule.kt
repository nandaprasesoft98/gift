package com.nandaprasetio.gift.di.module

import com.nandaprasetio.gift.data.datasource.giftdatasource.DefaultGiftDataSource
import com.nandaprasetio.gift.data.datasource.giftdatasource.GiftDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourceModule {
    @Binds
    @Singleton
    abstract fun bindDefaultGiftDataSource(defaultGiftDataSource: DefaultGiftDataSource): GiftDataSource
}