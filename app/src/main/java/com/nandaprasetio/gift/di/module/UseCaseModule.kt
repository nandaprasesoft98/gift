package com.nandaprasetio.gift.di.module

import com.nandaprasetio.gift.domain.usecase.addgifttowishlistusecase.AddGiftToWishlistUseCase
import com.nandaprasetio.gift.domain.usecase.addgifttowishlistusecase.DefaultAddGiftToWishlistUseCase
import com.nandaprasetio.gift.domain.usecase.getgiftdetailusecase.DefaultGetGiftDetailUseCase
import com.nandaprasetio.gift.domain.usecase.getgiftdetailusecase.GetGiftDetailUseCase
import com.nandaprasetio.gift.domain.usecase.getgiftlistusecase.DefaultGetGiftUseCase
import com.nandaprasetio.gift.domain.usecase.getgiftlistusecase.GetGiftUseCase
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
abstract class UseCaseModule {
    @Binds
    @ViewModelScoped
    abstract fun bindDefaultAddGiftToWishlistUseCase(
        defaultAddGiftToWishlistUseCase: DefaultAddGiftToWishlistUseCase
    ): AddGiftToWishlistUseCase

    @Binds
    @ViewModelScoped
    abstract fun bindDefaultGetGiftDetailUseCase(
        defaultGetGiftDetailUseCase: DefaultGetGiftDetailUseCase
    ): GetGiftDetailUseCase

    @Binds
    @ViewModelScoped
    abstract fun bindDefaultGetGiftUseCase(
        defaultGetGiftUseCase: DefaultGetGiftUseCase
    ): GetGiftUseCase
}