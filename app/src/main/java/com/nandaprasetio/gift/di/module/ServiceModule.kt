package com.nandaprasetio.gift.di.module

import com.nandaprasetio.gift.data.service.GiftService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ServiceModule {
    @Provides
    @Singleton
    fun provideGiftService(retrofit: Retrofit): GiftService {
        return retrofit.create(GiftService::class.java)
    }
}