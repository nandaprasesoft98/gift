package com.nandaprasetio.gift.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.nandaprasetio.gift.domain.usecase.getgiftlistusecase.GetGiftUseCase
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

@HiltViewModel
class GiftViewModel @Inject constructor(
    private val getGiftUseCase: GetGiftUseCase
): ViewModel() {
    fun getGiftPager(): Flow<PagingData<BaseModelValue>> {
        return getGiftUseCase.invoke().flow.cachedIn(viewModelScope)
    }
}