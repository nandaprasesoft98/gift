package com.nandaprasetio.gift.viewmodel

import androidx.lifecycle.*
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.domain.usecase.addgifttowishlistusecase.AddGiftToWishlistUseCase
import com.nandaprasetio.gift.domain.usecase.getgiftdetailusecase.GetGiftDetailUseCase
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GiftDetailViewModel @Inject constructor(
    private val getGiftDetailUseCase: GetGiftDetailUseCase,
    private val addGiftToWishlistUseCase: AddGiftToWishlistUseCase
): ViewModel() {
    private val wishlistStatusMutableLiveData: MutableLiveData<Int> = MutableLiveData()
    val wishlistStatusLiveData: LiveData<Int> = wishlistStatusMutableLiveData

    fun getGiftDetail(id: Int): Flow<PagingData<BaseModelValue>> {
        return getGiftDetailUseCase.invoke(id).flow.cachedIn(viewModelScope)
    }

    fun addGiftToWishlist(id: Int) {
        viewModelScope.launch {
            addGiftToWishlistUseCase.invoke(id).also {
                if (it is LoadDataResult.Success) {
                    wishlistStatusMutableLiveData.postValue(it.output)
                }
            }
        }
    }

    fun updateWishlistStatus(wishlistStatus: Int) {
        // Because the caller is exactly from epoxy build model thread,
        // so passing value through postValue will change from runnable main thread to main thread.
        wishlistStatusMutableLiveData.postValue(wishlistStatus)
    }
}