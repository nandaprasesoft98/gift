package com.nandaprasetio.gift

sealed class LoadDataResult<T> {
    data class Success<T>(val output: T): LoadDataResult<T>()
    data class Error<T>(val e: Throwable): LoadDataResult<T>()
}