package com.nandaprasetio.gift

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class GiftApplication: Application()