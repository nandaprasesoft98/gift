package com.nandaprasetio.gift

import androidx.paging.PagingConfig

object Config {
    val defaultPagingConfig: PagingConfig = PagingConfig(
        pageSize = 10,
        prefetchDistance = 6,
        initialLoadSize = 10
    )
}