package com.nandaprasetio.gift.presentation.epoxy.epoxymodel

import android.annotation.SuppressLint
import androidx.core.view.isVisible
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.nandaprasetio.gift.R
import com.nandaprasetio.gift.ext.setImageWithGlide
import com.nandaprasetio.gift.presentation.epoxy.epoxyholder.ImageWithNewBadgeItemEpoxyHolder
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.ImageWithNewBadgeItemModelValue

@SuppressLint("NonConstantResourceId")
@EpoxyModelClass(layout = R.layout.item_image_with_new_badge)
abstract class ImageWithNewBadgeItemEpoxyModel: EpoxyModelWithHolder<ImageWithNewBadgeItemEpoxyHolder>() {
    @EpoxyAttribute
    var imageWithNewBadgeItemModelValue: ImageWithNewBadgeItemModelValue? = null

    override fun bind(holder: ImageWithNewBadgeItemEpoxyHolder) {
        super.bind(holder)
        imageWithNewBadgeItemModelValue?.also {
            holder.giftImageView.setImageWithGlide(it.imageUrl)
            holder.newImageView.isVisible = it.isNew == 1
        }
    }
}