package com.nandaprasetio.gift.presentation.errorprovider

import android.content.Context

abstract class BaseErrorProvider {
    abstract fun getErrorProviderResult(e: Throwable, context: Context): ErrorProviderResult
}