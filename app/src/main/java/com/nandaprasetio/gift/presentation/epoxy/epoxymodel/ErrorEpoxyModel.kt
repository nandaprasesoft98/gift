package com.nandaprasetio.gift.presentation.epoxy.epoxymodel

import android.annotation.SuppressLint
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.nandaprasetio.gift.R
import com.nandaprasetio.gift.presentation.epoxy.epoxyholder.ErrorEpoxyHolder
import com.nandaprasetio.gift.presentation.errorprovider.ErrorProviderResult

@SuppressLint("NonConstantResourceId")
@EpoxyModelClass(layout = R.layout.item_title_and_description)
abstract class ErrorEpoxyModel: EpoxyModelWithHolder<ErrorEpoxyHolder>(), HasLeftRightPaddingEpoxyModel {
    @EpoxyAttribute
    var errorProviderResult: ErrorProviderResult? = null

    override fun bind(holder: ErrorEpoxyHolder) {
        super.bind(holder)
        errorProviderResult?.also {
            it.iconResId?.also { resId -> holder.errorImageView.setImageResource(resId) }
            holder.titleTextView.text = it.title
            holder.descriptionTextView.text = it.message
        }
    }
}