package com.nandaprasetio.gift.presentation.activity

import com.nandaprasetio.gift.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity: BaseActivity<ActivityMainBinding>() {
    override fun setViewBinding(): ActivityMainBinding {
        return ActivityMainBinding.inflate(this.layoutInflater)
    }

    override fun isEnableBack(): Boolean = false
}