package com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue

data class ImageWithNewBadgeItemModelValue(
    override val id: String?,
    val imageUrl: String?,
    val isNew: Int,
    override val tag: Any? = null
): BaseItemModelValue()