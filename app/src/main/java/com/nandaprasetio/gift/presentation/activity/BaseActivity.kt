package com.nandaprasetio.gift.presentation.activity

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewbinding.ViewBinding
import com.nandaprasetio.gift.R

abstract class BaseActivity<T: ViewBinding>: AppCompatActivity() {
    protected var viewBinding: T? = null

    protected abstract fun setViewBinding(): T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = setViewBinding()
        this.setContentView(viewBinding?.root)

        viewBinding?.root?.also { view ->
            // Toolbar
            val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
            val titleTextView = view.findViewById<TextView>(R.id.text_view_title)
            this.setSupportActionBar(toolbar)
            this.supportActionBar?.also {
                val enableBack = isEnableBack()
                titleTextView.text = setTitle()
                it.setDisplayShowTitleEnabled(false)
                it.setHomeButtonEnabled(enableBack)
                it.setDisplayShowHomeEnabled(enableBack)
                it.setDisplayHomeAsUpEnabled(enableBack)
                it.setBackgroundDrawable(ColorDrawable(Color.WHITE))
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            android.R.id.home -> { this.onBackPressed(); true }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewBinding = null
    }

    protected open fun setTitle(): CharSequence = this.getString(R.string.app_name)

    protected open fun isEnableBack(): Boolean = true
}