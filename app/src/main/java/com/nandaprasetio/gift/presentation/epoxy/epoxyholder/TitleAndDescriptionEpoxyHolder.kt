package com.nandaprasetio.gift.presentation.epoxy.epoxyholder

import android.widget.TextView
import com.nandaprasetio.gift.R

class TitleAndDescriptionEpoxyHolder: KotlinEpoxyHolder() {
    val titleTextView: TextView by bind(R.id.text_view_title)
    val descriptionTextView: TextView by bind(R.id.text_view_description)
}