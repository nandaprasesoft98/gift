package com.nandaprasetio.gift.presentation.fragment

import androidx.fragment.app.viewModels
import androidx.paging.PagingData
import com.nandaprasetio.gift.presentation.epoxy.epoxycontroller.BasePagingDataEpoxyController
import com.nandaprasetio.gift.presentation.epoxy.epoxycontroller.GiftPagingDataEpoxyController
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import com.nandaprasetio.gift.viewmodel.GiftViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.flow.Flow

@AndroidEntryPoint
@ObsoleteCoroutinesApi
class GiftPagingDataEpoxyFragment: BasePagingDataEpoxyFragment<BaseModelValue>() {
    private val giftViewModel: GiftViewModel by viewModels()

    override fun onGetPagingDataFlow(): Flow<PagingData<BaseModelValue>> {
        return giftViewModel.getGiftPager()
    }

    override fun onGetPagingDataEpoxyController(): BasePagingDataEpoxyController<BaseModelValue> {
        return GiftPagingDataEpoxyController()
    }
}