package com.nandaprasetio.gift.presentation.epoxy.epoxyholder

import android.widget.TextView
import com.nandaprasetio.gift.R

class ExpandedDescriptionEpoxyHolder: KotlinEpoxyHolder() {
    val descriptionTextView: TextView by bind(R.id.text_view_description)
    val expandTextView: TextView by bind(R.id.text_view_expand)
}