package com.nandaprasetio.gift.presentation.epoxy.epoxymodel

import android.annotation.SuppressLint
import androidx.core.view.isVisible
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.nandaprasetio.gift.R
import com.nandaprasetio.gift.presentation.epoxy.epoxyholder.SubTitleEpoxyHolder
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.SubTitleItemModelValue

@SuppressLint("NonConstantResourceId")
@EpoxyModelClass(layout = R.layout.item_sub_title)
abstract class SubTitleEpoxyModel: EpoxyModelWithHolder<SubTitleEpoxyHolder>(), HasLeftRightPaddingEpoxyModel {
    @EpoxyAttribute
    var subTitleItemModelValue: SubTitleItemModelValue? = null

    override fun bind(holder: SubTitleEpoxyHolder) {
        super.bind(holder)
        subTitleItemModelValue?.also {
            holder.subTitleTextView.isVisible = !it.subTitle.isNullOrBlank()
            holder.subTitleTextView.text = it.subTitle
        }
    }
}