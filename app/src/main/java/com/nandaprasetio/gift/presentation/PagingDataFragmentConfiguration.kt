package com.nandaprasetio.gift.presentation

import androidx.recyclerview.widget.RecyclerView

class PagingDataFragmentConfiguration(
    val itemSpacingDp: Int,
    val layoutManager: RecyclerView.LayoutManager
)