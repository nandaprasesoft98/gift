package com.nandaprasetio.gift.presentation.fragment

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.nandaprasetio.gift.databinding.FragmentRecyclerViewPagingBinding
import com.nandaprasetio.gift.presentation.PagingDataFragmentConfiguration
import com.nandaprasetio.gift.presentation.epoxy.epoxycontroller.BasePagingDataEpoxyController
import com.nandaprasetio.gift.presentation.errorprovider.DefaultErrorProvider
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@ObsoleteCoroutinesApi
abstract class BasePagingDataEpoxyFragment<T: BaseModelValue>: BaseFragment<FragmentRecyclerViewPagingBinding>() {
    private val defaultErrorProvider = DefaultErrorProvider()

    private lateinit var _basePagingDataEpoxyController: BasePagingDataEpoxyController<T>
    protected val basePagingDataEpoxyController: BasePagingDataEpoxyController<T>
        get() {
            return _basePagingDataEpoxyController
        }

    override fun setViewBinding(): FragmentRecyclerViewPagingBinding {
        return FragmentRecyclerViewPagingBinding.inflate(this.layoutInflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewBinding?.also { binding ->
            _basePagingDataEpoxyController = onGetPagingDataEpoxyController()
            val pagingDataFragmentConfiguration = onGetPagingDataFragmentConfiguration()
            _basePagingDataEpoxyController.addLoadStateListener {
                // Refresh
                val refresh = it.source.refresh
                binding.progressBarContent.isVisible = refresh is LoadState.Loading
                binding.recyclerViewContent.isVisible = refresh !is LoadState.Loading
                binding.layoutViewError.root.isVisible = refresh is LoadState.Error
                if (refresh is LoadState.Error) {
                    val errorProviderResult = defaultErrorProvider.getErrorProviderResult(refresh.error, view.context)
                    errorProviderResult.iconResId?.also { resId ->
                        binding.layoutViewError.imageViewError.setImageResource(resId)
                    }
                    binding.layoutViewError.textViewTitle.text = errorProviderResult.title
                    binding.layoutViewError.textViewDescription.text = errorProviderResult.message
                }

                // Append
                val append = it.source.append
                _basePagingDataEpoxyController.loading = append is LoadState.Loading
                _basePagingDataEpoxyController.error = if (append is LoadState.Error) {
                    defaultErrorProvider.getErrorProviderResult(append.error, view.context)
                } else {
                    null
                }
            }
            lifecycleScope.launch {
                onGetPagingDataFlow().collect {
                    _basePagingDataEpoxyController.submitData(it)
                }
            }
            binding.recyclerViewContent.also {
                it.layoutManager = pagingDataFragmentConfiguration.layoutManager.apply {
                    if (this is GridLayoutManager) {
                        this.spanSizeLookup = _basePagingDataEpoxyController.spanSizeLookup
                    }
                }
                it.setItemSpacingDp(pagingDataFragmentConfiguration.itemSpacingDp)
                it.setControllerAndBuildModels(_basePagingDataEpoxyController)
            }
        }
    }

    protected abstract fun onGetPagingDataFlow(): Flow<PagingData<T>>

    protected abstract fun onGetPagingDataEpoxyController(): BasePagingDataEpoxyController<T>

    protected open fun onGetPagingDataFragmentConfiguration(): PagingDataFragmentConfiguration {
        return PagingDataFragmentConfiguration(
            itemSpacingDp = 16,
            layoutManager = GridLayoutManager(this.requireContext(), 2, RecyclerView.VERTICAL, false),
        )
    }
}