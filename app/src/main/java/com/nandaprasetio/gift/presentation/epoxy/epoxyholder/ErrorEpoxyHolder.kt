package com.nandaprasetio.gift.presentation.epoxy.epoxyholder

import android.widget.ImageView
import android.widget.TextView
import com.nandaprasetio.gift.R

class ErrorEpoxyHolder: KotlinEpoxyHolder() {
    val errorImageView: ImageView by bind(R.id.image_view_error)
    val titleTextView: TextView by bind(R.id.text_view_title)
    val descriptionTextView: TextView by bind(R.id.text_view_description)
}