package com.nandaprasetio.gift.presentation.errorprovider

import androidx.annotation.DrawableRes

class ErrorProviderResult(
    val title: String?,
    val message: String?,
    val e: Throwable,
    @DrawableRes
    val iconResId: Int? = null
)