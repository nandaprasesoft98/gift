package com.nandaprasetio.gift.presentation.fragment

import android.os.Bundle
import android.os.Looper
import android.view.View
import androidx.fragment.app.viewModels
import androidx.paging.PagingData
import com.nandaprasetio.gift.ExtraAndArgument
import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.data.pagingsource.GiftDetailPagingSource
import com.nandaprasetio.gift.presentation.epoxy.epoxycontroller.BasePagingDataEpoxyController
import com.nandaprasetio.gift.presentation.epoxy.epoxycontroller.GiftDetailPagingDataEpoxyController
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.GiftDetailMainCarouselItemModelValue
import com.nandaprasetio.gift.viewmodel.GiftDetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ObsoleteCoroutinesApi
import kotlinx.coroutines.flow.Flow

@AndroidEntryPoint
@ObsoleteCoroutinesApi
class GiftDetailPagingDataEpoxyFragment: BasePagingDataEpoxyFragment<BaseModelValue>() {
    private val giftDetailViewModel: GiftDetailViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val basePagingDataEpoxyController = basePagingDataEpoxyController as GiftDetailPagingDataEpoxyController
        giftDetailViewModel.wishlistStatusLiveData.observe(this.viewLifecycleOwner) {
            println(Thread.currentThread())
            basePagingDataEpoxyController.wishlistStatus = it
        }
    }

    override fun onGetPagingDataFlow(): Flow<PagingData<BaseModelValue>> {
        return giftDetailViewModel.getGiftDetail(this.requireArguments().getInt(ExtraAndArgument.ARGUMENT_GIFT_ID))
    }

    override fun onGetPagingDataEpoxyController(): BasePagingDataEpoxyController<BaseModelValue> {
        return GiftDetailPagingDataEpoxyController(
            onWishlistClicked = { giftDetailViewModel.addGiftToWishlist(it) },
            buildItemModelCallback = {
                if (it is GiftDetailMainCarouselItemModelValue) {
                    giftDetailViewModel.updateWishlistStatus(it.addedToWishlist)
                }
            }
        )
    }
}