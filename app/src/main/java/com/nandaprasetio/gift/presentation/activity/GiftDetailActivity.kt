package com.nandaprasetio.gift.presentation.activity

import android.os.Bundle
import com.nandaprasetio.gift.ExtraAndArgument
import com.nandaprasetio.gift.R
import com.nandaprasetio.gift.databinding.ActivityGiftDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GiftDetailActivity: BaseActivity<ActivityGiftDetailBinding>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val fragment = this.supportFragmentManager.findFragmentById(R.id.fragment_container_view_gift_detail)
        fragment?.arguments = Bundle().also {
            it.putInt(ExtraAndArgument.ARGUMENT_GIFT_ID, this.intent.getIntExtra(ExtraAndArgument.EXTRA_GIFT_ID, 0))
        }
    }

    override fun setViewBinding(): ActivityGiftDetailBinding {
        return ActivityGiftDetailBinding.inflate(this.layoutInflater)
    }

    override fun setTitle(): CharSequence = this.getString(R.string.title_gift_detail)
}