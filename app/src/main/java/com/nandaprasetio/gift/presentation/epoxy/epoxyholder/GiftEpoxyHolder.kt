package com.nandaprasetio.gift.presentation.epoxy.epoxyholder

import android.view.View
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.nandaprasetio.gift.R

class GiftEpoxyHolder: KotlinEpoxyHolder() {
    val containerCardView: View by bind(R.id.card_view_container)
    val newImageView: ImageView by bind(R.id.image_view_new)
    val giftImageView: ImageView by bind(R.id.image_view_gift)
    val nameTextView: TextView by bind(R.id.text_view_name)
    val pointTextView: TextView by bind(R.id.text_view_point)
    val reviewTextView: TextView by bind(R.id.text_view_review)
    val giftRatingBar: RatingBar by bind(R.id.rating_bar_gift)
    val wishlistImageView: ImageView by bind(R.id.image_view_wishlist)
}