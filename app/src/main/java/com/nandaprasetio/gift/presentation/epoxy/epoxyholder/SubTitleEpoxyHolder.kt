package com.nandaprasetio.gift.presentation.epoxy.epoxyholder

import android.widget.TextView
import com.nandaprasetio.gift.R

class SubTitleEpoxyHolder: KotlinEpoxyHolder() {
    val subTitleTextView: TextView by bind(R.id.text_view_sub_title)
}