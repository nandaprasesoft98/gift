package com.nandaprasetio.gift.presentation.modelvalue

abstract class BaseModelValue {
    open val id: String? = null
    open val tag: Any? = null
}