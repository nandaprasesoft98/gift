package com.nandaprasetio.gift.presentation.epoxy.epoxymodel

import android.annotation.SuppressLint
import android.os.Build
import android.text.Html
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.nandaprasetio.gift.R
import com.nandaprasetio.gift.presentation.epoxy.epoxyholder.ExpandedDescriptionEpoxyHolder
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.ExpandedDescriptionItemModelValue

@SuppressLint("NonConstantResourceId")
@EpoxyModelClass(layout = R.layout.item_description_expanded)
abstract class ExpandedDescriptionEpoxyModel: EpoxyModelWithHolder<ExpandedDescriptionEpoxyHolder>(), HasLeftRightPaddingEpoxyModel {
    @EpoxyAttribute
    var expandedDescriptionItemModelValue: ExpandedDescriptionItemModelValue? = null

    @EpoxyAttribute
    var expanded: Boolean = false

    private var localExpanded: Boolean = false

    override fun bind(holder: ExpandedDescriptionEpoxyHolder) {
        super.bind(holder)
        localExpanded = expanded
        expandedDescriptionItemModelValue?.also { value ->
            bindText(holder, value)
            holder.expandTextView.setOnClickListener {
                localExpanded = !localExpanded
                bindText(holder, value)
            }
        }
    }

    private fun bindText(holder: ExpandedDescriptionEpoxyHolder, value: ExpandedDescriptionItemModelValue) {
        holder.descriptionTextView.text = value.description?.let { string ->
            val spanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(value.description, Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(value.description)
            }
            if (!localExpanded) { "${spanned.take(value.nonExpandedStringCount)}..." } else { spanned }
        }
        holder.expandTextView.text = if (!localExpanded) { "Read More" } else { "Read Less" }
    }
}