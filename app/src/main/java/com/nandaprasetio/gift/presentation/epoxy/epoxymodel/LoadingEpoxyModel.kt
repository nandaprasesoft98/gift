package com.nandaprasetio.gift.presentation.epoxy.epoxymodel

import android.annotation.SuppressLint
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.nandaprasetio.gift.R
import com.nandaprasetio.gift.presentation.epoxy.epoxyholder.LoadingEpoxyHolder

@SuppressLint("NonConstantResourceId")
@EpoxyModelClass(layout = R.layout.item_loading)
abstract class LoadingEpoxyModel: EpoxyModelWithHolder<LoadingEpoxyHolder>()