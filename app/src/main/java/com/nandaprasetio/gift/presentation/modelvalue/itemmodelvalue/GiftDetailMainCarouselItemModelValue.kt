package com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue

data class GiftDetailMainCarouselItemModelValue(
    override val id: String?,
    val rating: Float,
    val reviews: Int,
    val points: Int,
    val addedToWishlist: Int,
    override val tag: Any? = null
): BaseItemModelValue()