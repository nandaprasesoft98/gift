package com.nandaprasetio.gift.presentation.epoxy.epoxycontroller

import com.airbnb.epoxy.EpoxyModel
import com.nandaprasetio.gift.presentation.epoxy.epoxymodel.ExpandedDescriptionEpoxyModel_
import com.nandaprasetio.gift.presentation.epoxy.epoxymodel.GiftDetailMainCarouselEpoxyModel_
import com.nandaprasetio.gift.presentation.epoxy.epoxymodel.GiftEpoxyModel_
import com.nandaprasetio.gift.presentation.epoxy.epoxymodel.ImageWithNewBadgeItemEpoxyModel_
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.ExpandedDescriptionItemModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.GiftDetailMainCarouselItemModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.GiftItemModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.ImageWithNewBadgeItemModelValue
import kotlinx.coroutines.ObsoleteCoroutinesApi

@ObsoleteCoroutinesApi
class GiftDetailPagingDataEpoxyController(
    private val onWishlistClicked: ((Int) -> Unit)? = null,
    buildItemModelCallback: ((BaseModelValue?) -> Unit)? = null
): BasePagingDataEpoxyController<BaseModelValue>(false, buildItemModelCallback) {
    var wishlistStatus: Int = 0
        set(value) {
            field = value
            this.requestModelBuild()
        }

    override fun buildEachItemModel(currentPosition: Int, item: BaseModelValue?): EpoxyModel<*>? {
        return when (item) {
            is GiftItemModelValue -> {
                GiftEpoxyModel_().id(item.id)
                    .giftItemModelValue(item)
                    .spanSizeOverride { _, _, _ -> 1 }
            }
            is GiftDetailMainCarouselItemModelValue -> {
                GiftDetailMainCarouselEpoxyModel_().id(item.id)
                    .giftDetailMainCarouselItemModelValue(item)
                    .onWishlistClicked(onWishlistClicked)
                    .spanSizeOverride { _, _, _ -> this.spanCount }
            }
            is ExpandedDescriptionItemModelValue -> {
                ExpandedDescriptionEpoxyModel_().id(item.id)
                    .expandedDescriptionItemModelValue(item)
                    .expanded(false)
                    .spanSizeOverride { _, _, _ -> this.spanCount }
            }
            is ImageWithNewBadgeItemModelValue -> {
                ImageWithNewBadgeItemEpoxyModel_().id(item.id)
                    .imageWithNewBadgeItemModelValue(item)
                    .spanSizeOverride { _, _, _ -> this.spanCount }
            }
            else -> {
                null
            }
        }
    }

    override fun addModels(models: List<EpoxyModel<*>>) {
        val modelsMutableList: MutableList<EpoxyModel<*>> = mutableListOf()
        models.forEach {
            if (it is GiftDetailMainCarouselEpoxyModel_) {
                val oldModelValue = it.giftDetailMainCarouselItemModelValue
                oldModelValue?.also { modelValue ->
                    val newModelValue = modelValue.copy(addedToWishlist = wishlistStatus)
                    modelsMutableList.add(
                        GiftDetailMainCarouselEpoxyModel_().id(newModelValue.id)
                            .giftDetailMainCarouselItemModelValue(newModelValue)
                            .onWishlistClicked(onWishlistClicked)
                            .spanSizeOverride { _, _, _ -> this.spanCount }
                    )
                }
            } else {
                modelsMutableList.add(it)
            }
        }
        super.addModels(modelsMutableList)
    }
}