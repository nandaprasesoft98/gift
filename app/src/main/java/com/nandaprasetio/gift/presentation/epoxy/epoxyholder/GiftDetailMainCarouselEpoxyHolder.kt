package com.nandaprasetio.gift.presentation.epoxy.epoxyholder

import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.nandaprasetio.gift.R

class GiftDetailMainCarouselEpoxyHolder: KotlinEpoxyHolder() {
    val pointTextView: TextView by bind(R.id.text_view_point)
    val reviewTextView: TextView by bind(R.id.text_view_review)
    val giftRatingBar: RatingBar by bind(R.id.rating_bar_gift)
    val wishlistImageView: ImageView by bind(R.id.image_view_wishlist)
    val wishlistStatusTextView: TextView by bind(R.id.text_view_wishlist_status)
}