package com.nandaprasetio.gift.presentation.epoxy.epoxymodel

import android.annotation.SuppressLint
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.nandaprasetio.gift.R
import com.nandaprasetio.gift.presentation.epoxy.epoxyholder.GiftEpoxyHolder
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.SeparatorItemModelValue

@SuppressLint("NonConstantResourceId")
@EpoxyModelClass(layout = R.layout.item_separator)
abstract class SeparatorEpoxyModel: EpoxyModelWithHolder<GiftEpoxyHolder>() {
    @EpoxyAttribute
    var separatorItemModelValue: SeparatorItemModelValue? = null
}