package com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue

import com.nandaprasetio.gift.domain.entity.gift.Gift

data class GiftItemModelValue(
    override val id: String?,
    val gift: Gift
): BaseItemModelValue()