package com.nandaprasetio.gift.presentation.epoxy.epoxymodel

import android.annotation.SuppressLint
import android.content.res.ColorStateList
import android.os.Build
import androidx.core.content.ContextCompat
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.nandaprasetio.gift.R
import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.presentation.epoxy.epoxyholder.GiftDetailMainCarouselEpoxyHolder
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.GiftDetailMainCarouselItemModelValue
import java.lang.NumberFormatException

@SuppressLint("NonConstantResourceId")
@EpoxyModelClass(layout = R.layout.item_gift_detail_main_carousel)
abstract class GiftDetailMainCarouselEpoxyModel: EpoxyModelWithHolder<GiftDetailMainCarouselEpoxyHolder>() {
    @EpoxyAttribute
    var giftDetailMainCarouselItemModelValue: GiftDetailMainCarouselItemModelValue? = null

    @EpoxyAttribute
    var onWishlistClicked: ((Int) -> Unit)? = null

    override fun bind(holder: GiftDetailMainCarouselEpoxyHolder) {
        super.bind(holder)
        giftDetailMainCarouselItemModelValue?.also {
            val reviewText = "${it.reviews} reviews"
            val isWishlisted = it.addedToWishlist == 1
            val context = holder.giftRatingBar.context
            holder.giftRatingBar.setIsIndicator(true)
            holder.giftRatingBar.rating = it.rating
            holder.reviewTextView.text = reviewText
            holder.pointTextView.text = it.points.toString()
            holder.wishlistImageView.setOnClickListener { _ ->
                if (it.tag is Gift) { onWishlistClicked?.invoke(it.tag.id) }
            }
            holder.wishlistImageView.setImageResource(
                if (isWishlisted) { R.drawable.ic_love } else { R.drawable.ic_love_1 }
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.wishlistImageView.imageTintList = if (isWishlisted) {
                    ColorStateList.valueOf(
                        ContextCompat.getColor(context, R.color.pink)
                    )
                } else {
                    null
                }
            }
        }
    }
}