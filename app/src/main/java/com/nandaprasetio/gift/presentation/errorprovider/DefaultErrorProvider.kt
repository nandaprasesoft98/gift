package com.nandaprasetio.gift.presentation.errorprovider

import android.content.Context
import java.io.IOException

class DefaultErrorProvider: BaseErrorProvider() {
    override fun getErrorProviderResult(e: Throwable, context: Context): ErrorProviderResult {
        return when (e) {
            is IOException -> ErrorProviderResult("No Connection Internet", "No connection internet for now.", e)
            else -> ErrorProviderResult("Something Wrong", e.message, e)
        }
    }
}