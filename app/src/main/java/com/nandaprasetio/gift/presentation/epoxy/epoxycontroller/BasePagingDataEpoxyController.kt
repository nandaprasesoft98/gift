package com.nandaprasetio.gift.presentation.epoxy.epoxycontroller

import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.paging3.PagingDataEpoxyController
import com.nandaprasetio.gift.presentation.epoxy.epoxymodel.*
import com.nandaprasetio.gift.presentation.errorprovider.ErrorProviderResult
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.SeparatorItemModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.SubTitleItemModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.TitleAndDescriptionItemModelValue
import kotlinx.coroutines.ObsoleteCoroutinesApi

@ObsoleteCoroutinesApi
abstract class BasePagingDataEpoxyController<T: BaseModelValue>(
    private val withSeparatorOnTop: Boolean = true,
    private val buildItemModelCallback: ((T?) -> Unit)? = null
): PagingDataEpoxyController<T>() {
    var loading: Boolean = false
        set(value) {
            if (field != value) requestModelBuild()
            field = value
        }

    var error: ErrorProviderResult? = null
        set(value) {
            if (field != value) requestModelBuild()
            field = value
        }

    override fun buildItemModel(currentPosition: Int, item: T?): EpoxyModel<*> {
        buildItemModelCallback?.invoke(item)
        return onCheckDefaultModelValue(currentPosition, item)
            ?: buildEachItemModel(currentPosition, item)
            ?: throw IllegalStateException("EpoxyModel is null because maybe it's not suitable EpoxyModel.")
    }

    override fun addModels(models: List<EpoxyModel<*>>) {
        val modelsMutableList: MutableList<EpoxyModel<*>> = mutableListOf()
        if (withSeparatorOnTop) {
            modelsMutableList.add(
                SeparatorEpoxyModel_().id("separator-first")
                    .spanSizeOverride { _, _, _ -> this.spanCount }
            )
        }
        modelsMutableList.addAll(models)
        modelsMutableList.add(
            when {
                loading -> LoadingEpoxyModel_().id("loading-paging")
                    .spanSizeOverride { _, _, _ -> this.spanCount }
                error != null -> ErrorEpoxyModel_().id("error-last")
                    .spanSizeOverride { _, _, _ -> this.spanCount }
                else -> SeparatorEpoxyModel_().id("separator-last")
                    .spanSizeOverride { _, _, _ -> this.spanCount }
            }
        )
        super.addModels(modelsMutableList)
    }

    protected open fun onCheckDefaultModelValue(currentPosition: Int, item: T?): EpoxyModel<*>? {
        return when (item) {
            is SeparatorItemModelValue -> SeparatorEpoxyModel_().id(item.id)
                .spanSizeOverride { _, _, _ -> this.spanCount }
            is TitleAndDescriptionItemModelValue -> TitleAndDescriptionEpoxyModel_().id(item.id)
                .titleAndDescriptionItemModelValue(item)
                .spanSizeOverride { _, _, _ -> this.spanCount }
            is SubTitleItemModelValue -> SubTitleEpoxyModel_().id(item.id)
                .subTitleItemModelValue(item)
                .spanSizeOverride { _, _, _ -> this.spanCount }
            else -> null
        }
    }

    protected abstract fun buildEachItemModel(currentPosition: Int, item: T?): EpoxyModel<*>?
}