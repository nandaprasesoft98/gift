package com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue

data class ExpandedDescriptionItemModelValue(
    override val id: String?,
    val description: String?,
    val nonExpandedStringCount: Int,
    override val tag: Any? = null
): BaseItemModelValue()