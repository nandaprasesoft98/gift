package com.nandaprasetio.gift.presentation.epoxy.epoxyholder

import android.widget.ImageView
import android.widget.TextView
import com.nandaprasetio.gift.R

class ImageWithNewBadgeItemEpoxyHolder: KotlinEpoxyHolder() {
    val newImageView: ImageView by bind(R.id.image_view_new)
    val giftImageView: ImageView by bind(R.id.image_view_gift)
}