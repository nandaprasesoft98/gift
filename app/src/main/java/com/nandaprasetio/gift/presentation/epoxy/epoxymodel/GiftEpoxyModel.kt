package com.nandaprasetio.gift.presentation.epoxy.epoxymodel

import android.annotation.SuppressLint
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Build
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.nandaprasetio.gift.ExtraAndArgument
import com.nandaprasetio.gift.R
import com.nandaprasetio.gift.ext.setImageWithGlide
import com.nandaprasetio.gift.presentation.activity.GiftDetailActivity
import com.nandaprasetio.gift.presentation.epoxy.epoxyholder.GiftEpoxyHolder
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.GiftItemModelValue

@SuppressLint("NonConstantResourceId")
@EpoxyModelClass(layout = R.layout.item_gift)
abstract class GiftEpoxyModel: EpoxyModelWithHolder<GiftEpoxyHolder>(), HasLeftRightPaddingEpoxyModel {
    @EpoxyAttribute
    var giftItemModelValue: GiftItemModelValue? = null

    override fun bind(holder: GiftEpoxyHolder) {
        super.bind(holder)
        giftItemModelValue?.gift?.also {
            val giftAttribute = it.giftAttribute
            val pointText = "${giftAttribute.points} points"
            val reviewText = "${giftAttribute.numOfReviews} reviews"
            val context = holder.newImageView.context
            val isWishlisted = giftAttribute.isWishlist == 1
            holder.containerCardView.setOnClickListener { view ->
                Intent(view.context, GiftDetailActivity::class.java).also { intent ->
                    intent.putExtra(ExtraAndArgument.EXTRA_GIFT_ID, it.giftAttribute.id)
                    view.context.startActivity(intent)
                }
            }
            holder.giftImageView.setImageWithGlide(giftAttribute.images[0])
            holder.newImageView.isVisible = giftAttribute.isNew == 1
            holder.nameTextView.text = giftAttribute.name
            holder.pointTextView.text = pointText
            holder.reviewTextView.text = reviewText
            holder.giftRatingBar.rating = giftAttribute.rating
            holder.giftRatingBar.setIsIndicator(true)
            holder.wishlistImageView.setImageResource(
                if (isWishlisted) { R.drawable.ic_love } else { R.drawable.ic_love_1 }
            )
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.wishlistImageView.imageTintList = if (isWishlisted) {
                    ColorStateList.valueOf(
                        ContextCompat.getColor(context, R.color.pink)
                    )
                } else {
                    null
                }
            }
        }
    }
}