package com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue

data class TitleAndDescriptionItemModelValue(
    override val id: String?,
    val title: String?,
    val description: String?,
    override val tag: Any? = null
): BaseItemModelValue()