package com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue

data class SubTitleItemModelValue(
    override val id: String?,
    val subTitle: String?,
    override val tag: Any? = null
): BaseItemModelValue()