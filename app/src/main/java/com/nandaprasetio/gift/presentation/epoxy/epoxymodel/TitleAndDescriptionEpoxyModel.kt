package com.nandaprasetio.gift.presentation.epoxy.epoxymodel

import android.annotation.SuppressLint
import androidx.core.view.isVisible
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.nandaprasetio.gift.R
import com.nandaprasetio.gift.presentation.epoxy.epoxyholder.TitleAndDescriptionEpoxyHolder
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.TitleAndDescriptionItemModelValue

@SuppressLint("NonConstantResourceId")
@EpoxyModelClass(layout = R.layout.item_title_and_description)
abstract class TitleAndDescriptionEpoxyModel: EpoxyModelWithHolder<TitleAndDescriptionEpoxyHolder>(), HasLeftRightPaddingEpoxyModel {
    @EpoxyAttribute
    var titleAndDescriptionItemModelValue: TitleAndDescriptionItemModelValue? = null

    override fun bind(holder: TitleAndDescriptionEpoxyHolder) {
        super.bind(holder)
        titleAndDescriptionItemModelValue?.also {
            holder.titleTextView.isVisible = !it.title.isNullOrBlank()
            holder.titleTextView.text = it.title
            holder.descriptionTextView.isVisible = !it.title.isNullOrBlank()
            holder.descriptionTextView.text = it.description
        }
    }
}