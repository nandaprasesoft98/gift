package com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue

data class SeparatorItemModelValue(
    override val id: String?,
    override val tag: Any? = null
): BaseItemModelValue()