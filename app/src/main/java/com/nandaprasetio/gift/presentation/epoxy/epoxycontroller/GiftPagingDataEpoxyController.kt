package com.nandaprasetio.gift.presentation.epoxy.epoxycontroller

import com.airbnb.epoxy.EpoxyModel
import com.nandaprasetio.gift.presentation.epoxy.epoxymodel.GiftEpoxyModel_
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import com.nandaprasetio.gift.presentation.modelvalue.itemmodelvalue.GiftItemModelValue
import kotlinx.coroutines.ObsoleteCoroutinesApi

@ObsoleteCoroutinesApi
class GiftPagingDataEpoxyController: BasePagingDataEpoxyController<BaseModelValue>() {
    override fun buildEachItemModel(currentPosition: Int, item: BaseModelValue?): EpoxyModel<*>? {
        return if (item is GiftItemModelValue) {
            GiftEpoxyModel_().id("gift-${item.id}")
                .giftItemModelValue(item)
                .spanSizeOverride { _, _, _ -> 1 }
        } else {
            null
        }
    }
}