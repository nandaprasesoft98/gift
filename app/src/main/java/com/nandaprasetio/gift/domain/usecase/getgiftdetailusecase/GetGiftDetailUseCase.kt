package com.nandaprasetio.gift.domain.usecase.getgiftdetailusecase

import androidx.paging.Pager
import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue

interface GetGiftDetailUseCase {
    fun invoke(id: Int): Pager<Int, BaseModelValue>
}