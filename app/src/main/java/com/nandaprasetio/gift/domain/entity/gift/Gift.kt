package com.nandaprasetio.gift.domain.entity.gift

import com.google.gson.annotations.SerializedName

data class Gift(
    @SerializedName("id")
    val id: Int,
    @SerializedName("type")
    val type: String?,
    @SerializedName("attributes")
    val giftAttribute: GiftAttribute
)