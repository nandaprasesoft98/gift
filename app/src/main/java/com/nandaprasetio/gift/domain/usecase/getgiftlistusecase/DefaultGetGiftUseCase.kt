package com.nandaprasetio.gift.domain.usecase.getgiftlistusecase

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.RemoteMediator
import com.nandaprasetio.gift.Config
import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.data.pagingsource.GiftPagingSource
import com.nandaprasetio.gift.data.repository.giftrepository.GiftRepository
import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import javax.inject.Inject

class DefaultGetGiftUseCase @Inject constructor(
    private val giftRepository: GiftRepository
): GetGiftUseCase {
    override fun invoke(): Pager<Int, BaseModelValue> {
        return Pager(
            config = Config.defaultPagingConfig,
            pagingSourceFactory = { GiftPagingSource(giftRepository) }
        )
    }
}