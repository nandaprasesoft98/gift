package com.nandaprasetio.gift.domain.usecase.getgiftlistusecase

import androidx.paging.Pager
import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.domain.entity.pagingresult.PagingResult
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue

interface GetGiftUseCase {
    fun invoke(): Pager<Int, BaseModelValue>
}