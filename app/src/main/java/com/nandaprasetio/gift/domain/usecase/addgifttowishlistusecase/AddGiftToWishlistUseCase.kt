package com.nandaprasetio.gift.domain.usecase.addgifttowishlistusecase

import com.nandaprasetio.gift.LoadDataResult

interface AddGiftToWishlistUseCase {
    suspend fun invoke(id: Int): LoadDataResult<Int>
}