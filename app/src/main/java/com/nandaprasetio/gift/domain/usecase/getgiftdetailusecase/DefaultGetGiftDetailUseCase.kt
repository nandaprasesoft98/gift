package com.nandaprasetio.gift.domain.usecase.getgiftdetailusecase

import androidx.paging.Pager
import com.nandaprasetio.gift.Config
import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.data.pagingsource.GiftDetailPagingSource
import com.nandaprasetio.gift.data.pagingsource.GiftPagingSource
import com.nandaprasetio.gift.data.repository.giftrepository.GiftRepository
import com.nandaprasetio.gift.domain.entity.gift.Gift
import com.nandaprasetio.gift.presentation.modelvalue.BaseModelValue
import javax.inject.Inject

class DefaultGetGiftDetailUseCase @Inject constructor(
    private val giftRepository: GiftRepository
): GetGiftDetailUseCase {
    override fun invoke(id: Int): Pager<Int, BaseModelValue> {
        return Pager(
            config = Config.defaultPagingConfig,
            pagingSourceFactory = { GiftDetailPagingSource(giftRepository, id) }
        )
    }
}