package com.nandaprasetio.gift.domain.usecase.addgifttowishlistusecase

import com.nandaprasetio.gift.LoadDataResult
import com.nandaprasetio.gift.data.database.GiftWishlist
import com.nandaprasetio.gift.data.repository.giftrepository.GiftRepository
import java.lang.NullPointerException
import javax.inject.Inject

class DefaultAddGiftToWishlistUseCase @Inject constructor(
    private val giftRepository: GiftRepository
): AddGiftToWishlistUseCase {
    override suspend fun invoke(id: Int): LoadDataResult<Int> {
        return LoadDataResult.Success(
            when (val giftWishlistResult = giftRepository.getGiftWishlist(id)) {
                is LoadDataResult.Success -> { giftRepository.deleteGiftFromWishlist(id); 0 }
                is LoadDataResult.Error -> {
                    if (giftWishlistResult.e is NullPointerException) {
                        giftRepository.addGiftToWishlist(id); 1
                    } else {
                        giftRepository.deleteGiftFromWishlist(id); 0
                    }
                }
            }
        )
    }
}