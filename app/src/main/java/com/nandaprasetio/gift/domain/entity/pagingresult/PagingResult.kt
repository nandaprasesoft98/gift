package com.nandaprasetio.gift.domain.entity.pagingresult

import com.google.gson.annotations.SerializedName

data class PagingResult<T>(
    @SerializedName("meta")
    val pagingResultMetadata: PagingResultMetadata,
    @SerializedName("data")
    val data: List<T>
)