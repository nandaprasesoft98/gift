package com.nandaprasetio.gift.domain.entity.gift

import com.google.gson.annotations.SerializedName

data class GiftAttribute(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String?,
    @SerializedName("info")
    val info: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("points")
    val points: Int,
    @SerializedName("slug")
    val slug: String?,
    @SerializedName("stock")
    val stock: Int,
    @SerializedName("images")
    val images: List<String>,
    @SerializedName("isNew")
    val isNew: Int,
    @SerializedName("rating")
    val rating: Float,
    @SerializedName("numOfReviews")
    val numOfReviews: Int,
    @SerializedName("isWishlist")
    val isWishlist: Int
)