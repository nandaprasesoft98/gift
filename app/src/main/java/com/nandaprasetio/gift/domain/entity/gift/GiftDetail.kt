package com.nandaprasetio.gift.domain.entity.gift

import com.google.gson.annotations.SerializedName

data class GiftDetail(
    @SerializedName("data")
    val gift: Gift
)