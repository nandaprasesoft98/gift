package com.nandaprasetio.gift.domain.entity.pagingresult

import com.google.gson.annotations.SerializedName

data class PagingResultMetadata(
    @SerializedName("totalItems")
    val totalItems: Int,
    @SerializedName("currentPage")
    val currentPage: Int,
    @SerializedName("itemPerPage")
    val itemPerPage: Int,
    @SerializedName("totalPages")
    val totalPages: Int
)