package com.nandaprasetio.gift

object ExtraAndArgument {
    const val EXTRA_GIFT_ID = "extra.GIFT_ID"
    const val ARGUMENT_GIFT_ID = "argument.GIFT_ID"
}